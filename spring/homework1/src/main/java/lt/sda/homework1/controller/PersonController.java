package lt.sda.homework1.controller;

import lt.sda.homework1.model.business.Transaction;
import lt.sda.homework1.model.dto.PersonRegisterDTO;
import lt.sda.homework1.model.dto.PersonSimpleDataDTO;
import lt.sda.homework1.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/person")
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping
    public void registerPerson(@RequestBody PersonRegisterDTO personRegisterDTO) {
        this.personService.registerPerson(personRegisterDTO);
    }

    @PostMapping("/transaction")
    public void makeTransaction(@RequestBody Transaction transaction) {
        this.personService.makeTransaction(transaction);
    }

    @GetMapping(value = "/{name}/{surname}/money")
    public double personMoney(@PathVariable String name, @PathVariable String surname) {
        return this.personService.showPersonMoney(name, surname);
    }

    @GetMapping(value = "/money")
    public double personMoney() {
        return this.personService.totalMoneyAtBank();
    }

    @GetMapping
    public List<PersonSimpleDataDTO> getAllUsers() {
        return this.personService.listAllUsers();
    }

    @DeleteMapping(value = "/{name}/{surname}")
    public void deleteUser(@PathVariable String name, @PathVariable String surname) {
        this.personService.deleteUser(name, surname);
    }


}
