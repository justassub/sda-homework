package lt.sda.homework1.model.business;

public enum TransactionType {
    WITHDRAW, DEPOSIT
}
