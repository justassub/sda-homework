package lt.sda.homework1.component;

import lt.sda.homework1.validator.TransactionValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonBean {

    @Bean
    public TransactionValidator transactionValidator() {
        return new TransactionValidator();
    }
}
