package lt.sda.homework1.model.business;

import lombok.Value;

@Value
public class Person {
    String name;
    String surname;
    int age;
    Account account;
}
