package lt.sda.homework1.validator;

import lt.sda.homework1.model.business.Account;
import lt.sda.homework1.model.business.Transaction;

public class TransactionValidator {


    public void validateDeposit(Transaction transaction) {
        if (transaction.getMoneyAmount() <= 0) {
            throw new TransactionValidationException("For deposit your money must be possitive");
        }
    }

    public void validateWithdraw(Transaction transaction, Account account) {
        if (account.getMoney() < transaction.getMoneyAmount()) {
            throw new TransactionValidationException("User does not have enough money");

        }
    }

    private static class TransactionValidationException extends RuntimeException {
        public TransactionValidationException(String message) {
            super(message);
        }
    }
}
