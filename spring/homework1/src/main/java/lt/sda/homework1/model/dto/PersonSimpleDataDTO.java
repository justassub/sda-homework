package lt.sda.homework1.model.dto;

import lombok.Value;

@Value
public class PersonSimpleDataDTO {
    String name;
    String surname;
}
