package lt.sda.homework1.model.dto;

import lombok.Data;
import lombok.Value;

@Value
public class PersonRegisterDTO {
    String name;
    String surname;
    int age;
}
