package lt.sda.homework1.model.business;

import lombok.Data;

@Data
public class Account {
    private double money;

    public void addMoney(double money) {
        this.money += money;
    }
    public void withdrawMoney(double money) {
        this.money -= money;
    }
}
