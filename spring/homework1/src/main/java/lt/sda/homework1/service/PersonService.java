package lt.sda.homework1.service;

import lt.sda.homework1.model.business.Account;
import lt.sda.homework1.model.business.Person;
import lt.sda.homework1.model.business.Transaction;
import lt.sda.homework1.model.dto.PersonRegisterDTO;
import lt.sda.homework1.model.dto.PersonSimpleDataDTO;
import lt.sda.homework1.validator.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonService {
    private final List<Person> personList = new ArrayList<>();

    private final TransactionValidator transactionValidator;

    @Autowired
    public PersonService(TransactionValidator transactionValidator) {
        this.transactionValidator = transactionValidator;
    }

    public void registerPerson(PersonRegisterDTO personData) {
        Person person = new Person(personData.getName(), personData.getSurname(), personData.getAge(), new Account());
        personList.add(person);
    }

    public List<PersonSimpleDataDTO> listAllUsers() {
        return personList.stream()
                .map(u -> new PersonSimpleDataDTO(u.getName(), u.getSurname()))
                .collect(Collectors.toList());
    }

    public void makeTransaction(Transaction transaction) {
        Person person = getUserByNameAndSurname(transaction.getPersonSimpleDataDTO());
        switch (transaction.getTransactionType()) {
            case DEPOSIT:
                transactionValidator.validateDeposit(transaction);
                person.getAccount().addMoney(transaction.getMoneyAmount());
                break;
            case WITHDRAW:
                transactionValidator.validateWithdraw(transaction, person.getAccount());
                person.getAccount().withdrawMoney(transaction.getMoneyAmount());
                break;
        }
    }

    public double showPersonMoney(String name, String surname) {
        Person person = getUserByNameAndSurname(new PersonSimpleDataDTO(name,surname));
        return person.getAccount().getMoney();
    }

    public double totalMoneyAtBank() {
        return personList.stream()
                .mapToDouble(p -> p.getAccount().getMoney())
                .sum();
    }

    public void deleteUser(String name, String surname) {
        Person person = getUserByNameAndSurname(new PersonSimpleDataDTO(name,surname));
        this.personList.remove(person);
    }

    private Person getUserByNameAndSurname(PersonSimpleDataDTO personSimpleDataDTO) {
        return findUserByNameAndSurname(
                personSimpleDataDTO.getName(),
                personSimpleDataDTO.getSurname()
        );
    }

    private Person findUserByNameAndSurname(String name, String surname) {
        return personList.stream()
                .filter(u -> u.getName().equals(name) && u.getSurname().equals(surname))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("User not found"));
    }
}
