package lt.sda.homework1.model.business;

import lombok.Value;
import lt.sda.homework1.model.dto.PersonSimpleDataDTO;

@Value
public class Transaction {
    TransactionType transactionType;
    double moneyAmount;
    PersonSimpleDataDTO personSimpleDataDTO;
}
